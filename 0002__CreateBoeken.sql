use test;

create table Boeken 
(
Voornaam varchar(50) char set utf8mb4,
Familienaam varchar(80) char set utf8mb4,
Title varchar (255) char set utf8mb4,
Stad varchar (50) char set utf8mb4,
 Verschijningsjaar varchar (4),
 Uitgeverij varchar (80),
 Herdruk varchar (4),
 Comentaar varchar (1000)
);