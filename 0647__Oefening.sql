use aptunes;

drop procedure if exists MockAlbumReleasesLoop;

Delimiter $$
use aptunes $$
create procedure MockAlbumReleasesLoop(in extraReleases int)
Begin
declare counter int default 0;
declare success bool;

start transaction;

loop1: loop

call MockAlbumReleaseWithSuccess(success);

if succes = 1 then
set counter = counter + 1;
end if;
if counter = extraReleases then
leave loop1;
end if;

end loop;

commit ;
End $$
Delimiter ;

