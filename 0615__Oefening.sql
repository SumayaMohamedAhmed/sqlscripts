use modernways;
alter  view Auteursboeken
as
select Boeken.Id as Boek_Id, concat(Voornaam, ' ', Familienaam) as Auteurs, Titel as BoekTitel
from boeken
inner join publicaties  on Boeken.Id=publicaties.Boeken_Id
inner join personen on personen.id = publicaties.Personen_Id
