use aptunes;

drop procedure if exists DemonstrateHandlerOrder;

Delimiter $$
use aptunes $$
create procedure DemonstrateHandlerOrder()
Begin
declare random tinyint default 0;


declare continue handler for sqlstate '45002'
begin
select 'State 45002 opgevangen. Geen probleem.';
end;


declare continue handler for sqlexception 
begin
select 'Een algemene fout opgevangen.';
end;

set random  = floor(rand()  * 3) + 1;

if random = 1 then 
signal sqlstate '45001';
elseif random = 2 then
signal sqlstate '45002' ;
else 
signal sqlstate '45003';
end if;

End $$
Delimiter ;
call DemonstrateHandlerOrder();