use aptunes;
alter table Genres
add index NaamIdx (Naam)

/* voor de index (1,37036760) na de index (1,71349070) */

/*
-- oplossing
create index NaamIdx on Genres (Naam);
*/