use aptunes;

CREATE USER student 
IDENTIFIED BY 'ikbeneenstudent';

GRANT EXECUTE ON procedure aptunes.GetAlbumDuration
TO student;
