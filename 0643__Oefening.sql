use aptunes;

Delimiter $$
use aptunes $$

create procedure CreateAndReleaseAlbum(in titel varchar(100), in bands_Id int)
Begin

start transaction;

insert into Albums (Titel)
values(titel);

insert into AlbumReleases (Bands_Id, Albums_Id)
values(bands_Id, LAST_INSERT_ID());

commit;
End $$
Delimiter ;

-- het zelfde als de oplosing


