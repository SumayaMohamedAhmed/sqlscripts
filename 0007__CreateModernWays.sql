create database test;

use test;

create table MijnBoeken

(
Voornaam varchar (50) char set utf8mb4,
Familienaam varchar (50) char set utf8mb4,
Titel varchar (50) char set utf8mb4,
Stad varchar (50) char set utf8mb4,
Verscijningsjaar varchar (4),
Uitgeverij varchar (50) char set utf8mb4,
Herdruk varchar (4),
Commentaar varchar (50) char set utf8mb4
);

