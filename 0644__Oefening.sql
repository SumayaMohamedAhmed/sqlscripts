use aptunes;

drop procedure if exists MockAlbumRelease;

Delimiter $$
use aptunes $$
create procedure MockAlbumRelease()
Begin
declare numberOfAlbums int default 0;
declare numberOfBands int default 0;
declare randomAlbumId int default 0;
declare randomBandId int default 0;

start transaction;

select count(*)
into numberOfAlbums
from Albums;

select count(*)
into numberOfBands 
from Bands;

set randomAlbumId = floor(rand()*numberOfAlbums) + 1;
set randomBandId = floor(rand()* numberOfBands) + 1;

if (randomBandId,randomAlbumId) not in 
(select Bands_Id, Albums_Id from Albumreleases)
then
insert into Albumreleases
values(randomBandId,randomAlbumId );
end if;

commit ;
End $$
Delimiter ;
