create table Uitleningen
(
	StartDatum date,
    EindDatum date,
    Leden_Id int constraint FK_Uitleningen_leden foreign key (leden_Id) references leden(leden.id),
    Boeken_Id int constraint FK_Uitleningen_boeken foreign key (boeken_Id) references boeken(boeken.id)
);