-- er is geen model oplossing voor

-- Ik heb hier een table personen gemaakt, met twee kolomen.
use ModernWays;
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);

-- Hier voeg ik voornaam en achternaam toe met distinct erbij zodat het niet twee keer staat.
insert into Personen (Voornaam, Familienaam)
   select distinct Voornaam, Familienaam from Boeken;    
   

-- Hier heb ik een paar kolomen toegevoegd.
alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null);    
   
   
-- Hier heb ik de kolom personen_Id toegevoegd. 
-- Ik heb het ene keer gedaan met not null erbij, omdat ik geen fout melding gekregen heb.
alter table Boeken add Personen_Id int not null;



-- Update gedaan om de data van Boeken.personen_Id naar Prsonen.Id te kopieren.
update Boeken cross join Personen
    set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;
    
    
    -- Hier heb ik twee kolomen van de Boeken verwijderd.
    alter table Boeken drop column Voornaam,
    drop column Familienaam;
    
    
-- De laatste stap heb ik een foreign key toegevoegd.    
alter table Boeken add constraint fk_Boeken_Personen
   foreign key(Personen_Id) references Personen(Id);