use aptunes;
drop procedure if exists DangerousInsertAlbumreleases;

Delimiter $$
use aptunes $$
create procedure DangerousInsertAlbumreleases()
Begin
declare numberOfAlbums int default 0;
declare numberOfBands int default 0;


declare randomAlbum_id1 int default 0;
declare randomAlbum_id2 int default 0;
declare randomAlbum_id3 int default 0;

declare randomBand_id1 int default 0;
declare randomBand_id2 int default 0;
declare randomBand_id3 int default 0;
declare randomvalue tinyint default 0;
declare counter int default 0;

declare exit handler for sqlexception
Begin
rollback;
select 'Nieuwe releases konden niet worden toegevoegd';
End;

select count(*)
into numberOfAlbums
 from Albums;
 
select count(*)
into numberOfBands
 from Bands;
 
set randomAlbum_id1 = floor (rand()* numberOfAlbums) + 1;
set randomAlbum_id2  = floor (rand()* numberOfAlbums) + 1;
set randomAlbum_id3  = floor (rand()* numberOfAlbums) + 1;
 
set randomBand_id1 = floor (rand()* numberOfBands) + 1;
set randomBand_id2 = floor (rand()* numberOfBands) + 1;
set randomBand_id3 = floor (rand()* numberOfBands) + 1;

set randomvalue = floor (rand()*3) + 1;

 start transaction;
 
 insert into Albumreleases(Bands_Id, Albums_Id) 
 values(randomBand_id1,randomAlbum_id1),
		(randomBand_id2,randomAlbum_id2);
 
 if randomvalue = 1 then
 signal sqlstate '45000';
 end if;
 
insert into Albumreleases(Bands_Id, Albums_Id) 
values(randomBand_id3,randomAlbum_id3);

commit;

End $$
Delimiter ;

call DangerousInsertAlbumreleases();

select count(*) from albumreleases