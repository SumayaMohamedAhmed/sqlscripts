use aptunes;
drop procedure if exists GetAlbumDuration2;
Delimiter $$
use aptunes $$
create DEFINER=root@localhost procedure GetAlbumDuration2(in liedjesId smallint UNSIGNED, out totalDuration smallint UNSIGNED )
sql security definer
Begin
declare songDuration tinyint UNSIGNED default 0;
declare stop tinyint UNSIGNED default 0;
declare pass smallint UNSIGNED default 0;

 DECLARE cursorLiedjes
 CURSOR FOR select lengte from liedjes
inner join albums on albums.Id = liedjes.Albums_Id
where Albums_Id = liedjesId;


DECLARE CONTINUE HANDLER FOR NOT FOUND 
SET stop = 1;


open cursorLiedjes;
loop1: loop
	fetch cursorLiedjes into songDuration;
    
    if stop = 1 then    
    leave loop1;
    end if;
    
    set pass = (pass + songDuration );
    end loop; 
   
close cursorLiedjes;

set totalDuration  = pass;

End $$
Delimiter ;
