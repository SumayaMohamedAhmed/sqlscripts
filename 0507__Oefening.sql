use modernways;
create table Metingen
(
Tijdstip datetime not null,
Grootte smallint unsigned not null,
Marge float (3,2) not null
);