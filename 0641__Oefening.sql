use aptunes;

Delimiter $$
use aptunes $$

create procedure NumberOfGenres(OUT aantal tinyInt)
Begin
select count(*)
into aantal
from Genres;
End $$

Delimiter ;