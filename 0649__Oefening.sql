use aptunes;

drop procedure if exists DemonstrateHandlerOrder;

Delimiter $$
use aptunes $$
create procedure DemonstrateHandlerOrder()
Begin
declare random int default 0;

declare continue handler for sqlstate '45002'
select 'State 45002 opgevangen. Geen probleem.';

declare exit handler for sqlexception 
Begin
resignal set MESSAGE_TEXT = 'Ik heb mijn best gedaan!';
End;

set random  = floor (rand()  * 3) + 1;
if random = 1 then 
signal sqlstate '45001';
elseif random = 2 then
signal sqlstate '45002' ;
else 
signal sqlstate '45003';
end if;

End $$
Delimiter ;

call DemonstrateHandlerOrder();