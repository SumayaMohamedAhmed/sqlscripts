use aptunes;


Delimiter $$
use aptunes $$
create procedure GetLiedjes(IN song varchar(50))
Begin
select Titel
from Liedjes
where Titel like concat('%', song, '%');
End $$
Delimiter ;