use aptunes;

Delimiter $$
use aptunes $$

create procedure CleanupOldMemberships(IN datum date, OUT verwijderdAantal int)
Begin

DECLARE aantalvoor int DEFAULT 0 ;
select count(*)
into aantalvoor
from Lidmaatschappen;

Delete from Lidmaatschappen
where Einddatum < (datum);

select aantalvoor - count(*) 
into verwijderdAantal
from Lidmaatschappen;

End $$
Delimiter ;

/*
-- Mijn tweede manier ZONDER declair

use Aptunes;

delimiter $$
use Aptunes $$
create procedure CleanupOldMemberships2(in datum date, out AantalVerwijderd int)
begin

select count(*) as AantalVoorDelete from Lidmaatschappen;

delete from Lidmaatschappen 
where Einddatum < datum;

select AantalVoorDelete - count(*) 
into AantalVerwijderd
from Lidmaatschappen;

end $$
delimiter ;

call CleanupOldMemberships('1945-05-05', @numberCleaned);

select @numberCleaned;

*/

/*
 oplossing van de leerkracht
Delimiter $$
use Aptunes $$

create procedure CleanupOldMemberships(IN datum date, OUT verwijderdAantal int)
Begin

start transactie;
select count(*)
into verwijderdAantal
from Lidmaatschappen
where einddatum is not null and einddatum < datum;

Delete from Lidmaatschappen
where Einddatum is not null and Einddatum < datum;
commit;
End $$
Delimiter ;



*/