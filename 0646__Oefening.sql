use aptunes;

drop procedure if exists MockAlbumReleases;

Delimiter $$
use aptunes $$
create procedure MockAlbumReleases(in extraReleases int)
Begin
declare counter int default 0;
declare success bool;

start transaction;

repeat
call MockAlbumReleaseWithSuccess(success);
if success = 1 then
set counter = counter + 1;
end if; 
until (extraReleases = counter)
end repeat;

commit ;
End $$
Delimiter ;
