Create database Sportclub;

use Sportclub;

create table Leden
(
Id int auto_increment primary key,
Voornaam varchar(50) not null
);

create table Taken
(
Id int auto_increment primary key,
Omschrijving varchar(50) not null
);

