use aptunes;

alter table Muzikanten
add index FamilienaamVoornaamIdx (Familienaam,Voornaam)

/* voor de index (0,22970000) na de index (0,17175770) */

/*
-- oplossing
create index on FamilienaamVoornaamIdx (Familienaam,Voornaam)

*/