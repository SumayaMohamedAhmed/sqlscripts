use ModernWays;
select Id from Studenten
inner join Evaluaties on Studenten.Id = Evaluaties.Studenten_Id
group by Studenten.Id
having avg(Cijfer) > (select avg(Cijfer) from Evaluaties)

-- oplossing van de leerkracht 
/*
select Studenten_Id as Id from Evaluaties 
group by Studenten_Id
having avg(Cijfer) > (select avg(Cijfer) from Evaluaties)

de opdracht was met inner join te doen.
*/