
create table Games 
(
	Id int auto_increment primary key,
	Titel varchar(50) char set utf8mb4 not null
);

create table Platformen
(
	Id int auto_increment primary key,
    Naam varchar(50) char set utf8mb4 not null
);

create table Releases
(
	Id int auto_increment primary key,
	Games_Id int, constraint FK_Releases_Games foreign key (Games_Id) references Games(Id),
    Platformen_Id int, constraint FK_Releases_Platformen foreign key (Platformen_Id) references Platformen(Id)    
);

